# Lab5 -- Integration testing

## Specs

| Spec | Value |
| - | - |
| Budet car price per minute | 24 |
| Luxury car price per minute | 57 |
| Fixed price per km | 19 |
| Allowed deviations in % | 10 |
| Inno discount in % | 18 |

## BVA

| Parameter | Possible values |
| - | - |
| Type | `budget`, `luxury`, `nonsense` |
| Plan | `minute`, `fixed_price`, `nonsense` |
| Distance | `100`, `-10`, `0`, `10`, `100000`, `nonsense` |
| Planned distance | `100`, `-10`, `0`, `10`, `100000`, `nonsense` |
| Time | `100`, `-10`, `0`, `10`, `100000`, `nonsense` |
| Planned time | `100`, `-10`, `0`, `10`, `100000`, `nonsense` |
| InnoDiscount | `yes`, `no`, `nonsense` |

## Bugs
- Incorrectly counts price for the `budget` type
- Valid result with distance, planned distance, time, planned time less than or equal to 0
- Valid result with `nonsense` inserted into distance, planned distance, time, planned time
- Counts price as `null` when taking `nonsense` as deciding input

## Table

| Type | Plan | Distance | Planned Distance | Time | Planned time | InnoDiscount | Expected | Actual |
| - | - | - | - | - | - | - | - | - |
| budget | minute | 100 | 100 | 100 | 100 | yes | 1968 | {"price":2558.4} |
| budget | fixed_price | 100 | 100 | 100 | 100 | yes | 1558 | {"price":1025} |
| budget | nonsense | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | 100 | 100 | yes | 4674 | {"price":4674} |
| luxury | fixed_price | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| luxury | nonsense | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| nonsense | minute | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| nonsense | fixed_price | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| nonsense | nonsense | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| budget | minute | -10 | -10 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | -10 | 0 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | -10 | 10 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | -10 | 100000 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | -10 | nonsense | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 0 | -10 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 0 | 0 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 0 | 10 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 0 | 100000 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 0 | nonsense | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 10 | -10 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 10 | 0 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 10 | 10 | 100 | 100 | yes | 1968 | {"price":2558.4} |
| budget | minute | 10 | 100000 | 100 | 100 | yes | 1968 | {"price":2558.4} |
| budget | minute | 10 | nonsense | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 100000 | -10 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 100000 | 0 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 100000 | 10 | 100 | 100 | yes | 1968 | {"price":2558.4} |
| budget | minute | 100000 | 100000 | 100 | 100000 | yes | 1968 | {"price":2558.4} |
| budget | minute | 100000 | nonsense | 100 | nonsense | yes | Invalid Request | {"price":2558.4} |
| budget | minute | nonsense | -10 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | nonsense | 0 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | nonsense | 10 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | nonsense | 100000 | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | nonsense | nonsense | 100 | 100 | yes | Invalid Request | {"price":2558.4} |
| budget | minute | 100 | 100 | -10 | -10 | yes | Invalid Request | Invalid Request |
| budget | minute | 100 | 100 | -10 | 0 | yes | Invalid Request | Invalid Request |
| budget | minute | 100 | 100 | -10 | 10 | yes | Invalid Request | Invalid Request |
| budget | minute | 100 | 100 | -10 | 100000 | yes | Invalid Request | Invalid Request |
| budget | minute | 100 | 100 | -10 | nonsense | yes | Invalid Request | Invalid Request |
| budget | minute | 100 | 100 | 0 | -10 | yes | Invalid Request | {"price":0} |
| budget | minute | 100 | 100 | 0 | 0 | yes | Invalid Request | {"price":0} |
| budget | minute | 100 | 100 | 0 | 10 | yes | Invalid Request | {"price":0} |
| budget | minute | 100 | 100 | 0 | 100000 | yes | Invalid Request | {"price":0} |
| budget | minute | 100 | 100 | 0 | nonsense | yes | Invalid Request | {"price":0} |
| budget | minute | 100 | 100 | 10 | -10 | yes | Invalid Request | {"price":255.84000000000003} |
| budget | minute | 100 | 100 | 10 | 0 | yes | Invalid Request | {"price":255.84000000000003} |
| budget | minute | 100 | 100 | 10 | 10 | yes | 196.8 | {"price":255.84000000000003} |
| budget | minute | 100 | 100 | 10 | 100000 | yes | 196.8 | {"price":255.84000000000003} |
| budget | minute | 100 | 100 | 10 | nonsense | yes | Invalid Request | {"price":255.84000000000003} |
| budget | minute | 100 | 100 | 100000 | -10 |  yes | Invalid Request | {"price":2558400} |
| budget | minute | 100 | 100 | 100000 | 0 | yes | Invalid Request | {"price":2558400} |
| budget | minute | 100 | 100 | 100000 | 10 | 100 | yes | 1968000 | {"price":2558400} |
| budget | minute | 100 | 100 | 100000 | 100000 | yes | 1968000 | {"price":2558400} |
| budget | minute | 100 | 100 | 100000 | nonsense | yes | Invalid Request | {"price":2558400} |
| budget | minute | 100 | 100 | nonsense | -10 | yes | Invalid Request | {"price":null} |
| budget | minute | 100 | 100 | nonsense | 0 | yes | Invalid Request | {"price":null} |
| budget | minute | 100 | 100 | nonsense | 10 | yes | Invalid Request | {"price":null} |
| budget | minute | 100 | 100 | nonsense | 100000 | yes | Invalid Request | {"price":null} |
| budget | minute | 100 | 100 | nonsense | nonsense | yes | Invalid Request | {"price":null} |
| budget | minute | 100 | 100 | 100 | 100 | yes | 1968 | {"price":2558.4} |
| budget | minute | 100 | 100 | 100 | 100 | no | 2400 | {"price":3120} |
| luxury | minute | 100 | 100 | 100 | 100 | no | 5700 | {"price":5700} |
| budget | minute | 100 | 100 | 100 | 100 | nonsense | Invalid Request | Invalid Request |
